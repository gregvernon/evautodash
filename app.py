import thread
import time
import Tkinter
import rospy
from geometry_msgs.msg import *

from std_msgs.msg import Float32, Float32MultiArray
from sensor_msgs.msg import NavSatFix, TimeReference
from nav_msgs.msg import Odometry

try:
    from pcan import CANMsg
except ImportError:
    print "Cannot import PCAN messages!"

try:
    from focus_serial.msg import SerialMsg
except ImportError:
    print "Cannot import Serial messages!"

app = Tkinter.Tk()

app.minsize(height=0, width=500)

class GUISubscriber:
    #_hwnd (GUI window handle)
    #_lt (time last message received)

    def __init__(self, hwnd, timeout=1.5):
        # template method

        # set the handle
        self._hwnd = hwnd
        # set the timeout
        self._timeout = timeout
        # do not allow updates until a message is received
        self._allow_update = False #can't make things private, unfortunately
        # setup widgets
        self._setWidgets()
        # subscribe to the topic
        self._doSubscribe()
        # update the message statistics
        #self._hwnd.after(100, self._updateMessageStatistics)

    def _setWidgets(self):
        self._lbl_sta = Tkinter.Label(self._hwnd, text='Not Receiving Messages', fg='Red')
        self._lbl_sta.pack()

        self._lbl_pub = Tkinter.Label(self._hwnd, text='0 Publishers', fg='Red')
        self._lbl_pub.pack()

    def _doSubscribe(self):
        self._t = time.time() - self._timeout

    def _handleMessage(self, d):
        newTime = time.time()

        if ((newTime - self._t) > self._timeout) and (not self._allow_update):
            self._allow_update = True
            self._hwnd.after(50, self._updateMessageStatistics)

        self._t = newTime

    def _updateMessageStatistics(self):
        if time.time() - self._t > self._timeout:
            self._allow_update = False
            self._lbl_sta.config(text='Not Receiving Messages', fg='Red')
            self._lbl_pub.config(fg='Red')
        else:
            self._lbl_sta.config(text='Receiving Messages', fg='Green')
            self._lbl_pub.config(fg='Green')
            self._hwnd.after(50, self._updateMessageStatistics)

    def TimeoutErr(self):
        return not self._allow_update  

class MyPiksi(GUISubscriber):
    #_t_gpsFix (last gps fix message)
    #_t_rtkFix (last rtk fix message)
    #_t_gpsTime (last gps time message)
    #_lbl_hdr (header Label handle)
    #_lbl_tm (last message received label)
    #_lbl_cnt_pub (label showing number of publishers)
    #_sub (rospy subscriber)

    def __init__(self, hwnd):
        GUISubscriber.__init__(self, hwnd)

    def _doSubscribe(self):
        GUISubscriber._doSubscribe(self)
        #self._t = time.time()
        self._sub_fix = rospy.Subscriber('gps/fix', NavSatFix, self._handleMessage)
        self._sub_rtk = rospy.Subscriber('gps/rtkfix', Odometry, self._handleMessage)
        self._sub_time = rospy.Subscriber('gps/time', TimeReference, self._handleMessage)
        
    def _setWidgets(self):
        GUISubscriber._setWidgets(self)
        self._lbl_pub.config(text="0 publishers on 3 topics")

    def _updateMessageStatistics(self):
        GUISubscriber._updateMessageStatistics(self)
        # force continual refreshing
        #self._hwnd.after(100, self._updateMessageStatistics)
        # get time since last message
        #t = time.time() - self._t

        # get current number of publishers
        n_pub = self._sub_rtk.get_num_connections()
        n_pub = n_pub + self._sub_fix.get_num_connections()
        n_pub = n_pub + self._sub_time.get_num_connections()
        self._lbl_pub.config(text=str(n_pub) + " publishers on 3 topics.")

class MyPCan(GUISubscriber):
    def __init__(self, hwnd):
        GUISubscriber.__init__(self, hwnd)

    def _doSubscribe(self):
        GUISubscriber._doSubscribe(self)

        try:
            CANMsg
        except NameError:
            print "Cannot start PCAN listener.  CANMsg not defined"
            self._lbl_sta.config(text='Error State!')
            self._lbl_pub.config(text='CANMsg not defined!')
            self._sub = False
        else:
            self._sub = rospy.Subscriber('can_data', CANMsg, self._handleMessage)
    
    def _updateMessageStatistics(self):
        GUISubscriber._updateMessageStatistics(self)
        #deltaT = time.time() - self._t
        
        if self._sub:
            n_pub = self._sub.get_num_connections()
            self._lbl_pub.config(text=str(n_pub) + " Publishers")

class MySerial(GUISubscriber):
    def __init__(self, hwnd):
        GUISubscriber.__init__(self, hwnd)

    def _doSubscribe(self):
        GUISubscriber._doSubscribe(self)

        try:
            SerialMsg
        except NameError:
            print "Cannot start serial monitor.  SerialMsg not defined!"
            self._lbl_sta.config(text="Error State!")
            self._lbl_pub.config(text="SerialMsg not defined!")
            self._sub = False
        else:
            self._sub = rospy.Subscriber('serial_data', SerialMsg, self._handleMessage)

    def _updateMessageStatistics(self):
        GUISubscriber._updateMessageStatistics(self)

        if self._sub:
            self._lbl_pub.config(text=str(self._sub.get_num_connections()) + " Publishers")

class MyKalman(GUISubscriber):
    def __init__(self, hwnd):
        GUISubscriber.__init__(self, hwnd)

    def _doSubscribe(self):
        GUISubscriber._doSubscribe(self)
        self._sub = rospy.Subscriber('filter_output', Odometry, self._handleMessage)
        
    def _updateMessageStatistics(self):
        GUISubscriber._updateMessageStatistics(self)

        n_pub = self._sub.get_num_connections()
        self._lbl_pub.config(text=str(n_pub) + " Publishers")


class MyLatLon(GUISubscriber):
    def __init__(self, hwnd):
        GUISubscriber.__init__(self, hwnd)
    
    def _doSubscribe(self):
        GUISubscriber._doSubscribe(self)
        self._lat_sub = rospy.Subscriber('lateral_command', Float32, self._handleMessage)
        self._lon_sub = rospy.Subscriber('longitudinal_commands', Float32MultiArray, self._handleMessage)

    def _setWidgets(self):
        GUISubscriber._setWidgets(self)
        self._lbl_pub.config(text='0 Publishers on 2 Topics')

    def _updateMessageStatistics(self):
        GUISubscriber._updateMessageStatistics(self)
        n_pub = self._lat_sub.get_num_connections() + self._lon_sub.get_num_connections()
        self._lbl_pub.config(text=str(n_pub) + "Publishers on 2 Topics")

class MyDesiredValues(GUISubscriber):
    def __init__(self, hwnd):
        GUISubscriber.__init__(self, hwnd)
    
    def _doSubscribe(self):
        GUISubscriber._doSubscribe(self)
        self._vel_sub = rospy.Subscriber('desired_velocity', Float32, self._handleVelocityMessage)
        self._ang_sub = rospy.Subscriber('desired_angle', Float32, self._handleAngleMessage)

    def _setWidgets(self):
        GUISubscriber._setWidgets(self)
        self._lbl_pub.config(text='0 Publishers on 2 Topics')

        self._ha = Tkinter.Label(self._hwnd, text='Desired Angle')
        self._ha.pack()
        self._lbl_ang = Tkinter.Label(self._hwnd, text=str(round(0, 10)))
        self._lbl_ang.pack()

        self._hv = Tkinter.Label(self._hwnd, text="Desired Velocity")
        self._hv.pack()

        self._lbl_vel = Tkinter.Label(self._hwnd, text=str(round(0, 10)))
        self._lbl_vel.pack()

    def _updateMessageStatistics(self):
        GUISubscriber._updateMessageStatistics(self)
        n_pub = self._vel_sub.get_num_connections() + self._ang_sub.get_num_connections()
        self._lbl_pub.config(text=str(n_pub) + " Publishers on 2 Topics")

    def _handleVelocityMessage(self, d):
        GUISubscriber._handleMessage(self, d)
        self._lbl_vel.config(text=str(round(d.data, 10)))

    def _handleAngleMessage(self, d):
        GUISubscriber._handleMessage(self, d)
        self._lbl_ang.config(text=str(round(d.data, 10)))

class MyTargetGPS(GUISubscriber):
    def __init__(self, hwnd):
        GUISubscriber.__init__(self, hwnd)

    def _doSubscribe(self):
        GUISubscriber._doSubscribe(self)
        self._sub = rospy.Subscriber('target_gps_data', Odometry, self._handleMessage)

    def _updateMessageStatistics(self):
        GUISubscriber._updateMessageStatistics(self)
        self._lbl_pub.config(text=str(self._sub.get_num_connections()) + " Publishers")


leftPane = Tkinter.PanedWindow(orient=Tkinter.VERTICAL, width=250)
leftPane.pack(fill=Tkinter.BOTH, expand=True, side=Tkinter.LEFT)

rightPane = Tkinter.PanedWindow(orient=Tkinter.VERTICAL, width=250)
rightPane.pack(fill=Tkinter.BOTH, expand=True, side=Tkinter.RIGHT)

#setup the piksi widgets
myPiksiFrame = Tkinter.LabelFrame(text="Piksi")
myPiksiFrame.pack(fill=Tkinter.X, expand=False)
myPiksi = MyPiksi(myPiksiFrame)
leftPane.add(myPiksiFrame)

#setup the pcan widgets
myPcanFrame = Tkinter.LabelFrame(text="PCAN")
myPcanFrame.pack(fill=Tkinter.X, expand=False)
myPCan = MyPCan(myPcanFrame)
leftPane.add(myPcanFrame)

# setup the serial monitor
mySerialFrame = Tkinter.LabelFrame(text="Serial")
mySerialFrame.pack(fill=Tkinter.X, expand=False)
mySerial = MySerial(mySerialFrame)
leftPane.add(mySerialFrame)

myFill = Tkinter.Frame()
myFill.pack()
leftPane.add(myFill)

#kalman filter
myKalmanFrame = Tkinter.LabelFrame(text="Kalman Filter")
myKalmanFrame.pack(fill=Tkinter.X, expand=False)
myKalman = MyKalman(myKalmanFrame)
rightPane.add(myKalmanFrame)

# Lat/Lon Controller
myLatLonFrame = Tkinter.LabelFrame(text="Lateral & Longitudinal Controller")
myLatLonFrame.pack(fill=Tkinter.X, expand=False)
myLatLon = MyLatLon(myLatLonFrame)
rightPane.add(myLatLonFrame)

# Desired Values
myDesiredFrame = Tkinter.LabelFrame(text="Desired Values")
myDesiredFrame.pack(fill=Tkinter.X, expand=False)
myDesiredValues = MyDesiredValues(myDesiredFrame)
rightPane.add(myDesiredFrame)

# Target GPS
myTargetGPSFrame = Tkinter.LabelFrame(text="Target GPS")
myTargetGPSFrame.pack(fill=Tkinter.X, expand=False)
myTargetGPS = MyTargetGPS(myTargetGPSFrame)
rightPane.add(myTargetGPSFrame)

rospy.init_node('system_status', anonymous=True)

app.mainloop()
